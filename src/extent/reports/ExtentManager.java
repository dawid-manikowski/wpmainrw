package extent.reports;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import main.Main;
import utils.PfUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class ExtentManager {

    public static ExtentReports reports;
    public static ExtentTest test;

    private static final String fileName = (new StringBuilder())
            .append(Main.workspacePath)
            .append("result")
            .append(System.getProperty("file.separator"))
            .append("attachments")
            .append(System.getProperty("file.separator"))
            .append("service.log").toString();

    private static List<String> listResult = new ArrayList<String>();

    public ExtentManager(String reportPathFileName){
        this.reports = new ExtentReports(reportPathFileName);
    }

    public void startTest(String testName){
        this.test = this.reports.startTest(testName);
    }

    /**
     * Załadowanie kofiguracji reportera z pliku.
     * @param fileName nazwa pliku z konfiguracją
     */
    public static void loadConfig(String fileName) {
        if (PfUtils.fileExist(fileName))
            reports.loadConfig(new File(fileName));
    }

    /**
     * Dodanie wpisu informacyjnego (INFO).
     */
    public static void logInfo(String msg) {
        listResult.add(clearHtml(msg));
        test.log(LogStatus.INFO, msg);
        writeTechnicalLog("[INFO] :  " + msg);
    }

    /**
     * Dodanie statusu pozytywnego (<i>PASS</i>).<br>
     */
    public static void logPass(String msg) {
        listResult.add("[PASS]" + clearHtml(msg));
        test.log(LogStatus.PASS, msg);
        writeTechnicalLog("[PASS] :  " + msg);
    }

    /**
     * Dodanie statusu negatywnego (<i>FAIL</i>).<br>
     */
    public static void logFail(String msg) {
        listResult.add("[FAIL]" + clearHtml(msg));
        test.log(LogStatus.FAIL, msg);
        writeTechnicalLog("[FAIL] :  " + msg);
    }

    /**
     * Wpis do logu technicznego
     */
    public static void writeTechnicalLog(String str) {
        BufferedWriter writer ;
        try {
            writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.append(PfUtils.getCurrentDateTime("timestamps") + str);
            writer.newLine();
            writer.close();
        } catch (Exception e) {}
    }

    /**
     * Dodanie statusu negatywnego (<i>FAIL</i>) tylko do loga technicznego.<br>
     */
    public static String logTechnicalFail(String msg) {
        listResult.add("[FAIL]" + clearHtml(msg));
        writeTechnicalLog("[FAIL] :  " + msg);
        return msg;
    }

    /**
     * Dodanie zrzutu ekranowego do raportu.
     * @param imagePath
     * 		ścieżka z nazwą pliku ze zrzutem ekranowym
     * @param
     * 		status w raporcie do zrzutu ekranowego
     */
    public void logScreenShot(String imagePath, LogStatus status) {
        if (test != null)
            test.log(status, test.addScreenCapture(imagePath));
    }

    public void closeReport() {
        reports.endTest(test);
        reports.flush();
    }


    /*==============================================================================================
		UTILS
	=============================================================================================*/


    /**
     * Usunięcie tagów HTML i wcinki kolejnych wierszy do logu tekstowego.
     * @param msg
     * 		treść do konwersji
     * @return wyfiltrowana treść
     */
    public static String clearHtml(String msg) {
        if (msg != null)
            return msg.replaceAll("<br>|<p (.*?)>|<div(.*?)", "\n").
                    replaceAll("</p>|</div>|<font(.*?)>|</font>|<i>|</i>|<b>|</b>|<strong>|</strong>|<pre>|</pre>", "");
        return msg;
    }

    /**
     * Globalna obsługa błędow stackTrace'a.<br>
     * Wypisanie stackTrace'a na ekrania, wpis treści stackTrace'a do logu.
     * @param e
     * 		obiekt wyjątku <pre>Exception</pre>
     */
    public String exceptionHandler(Throwable e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        writeTechnicalLog("[ERROR] :  " + sw.toString());
        logFail(e.getMessage());
        return sw.toString();
    }

}
