package main;

import extent.reports.ExtentManager;
import org.json.JSONException;
import org.testng.IConfigurationListener;
import org.testng.ITestResult;
import utils.PfUtils;

import java.io.IOException;


public class ConfigurationListener implements IConfigurationListener {
	
	
	@Override
	public void onConfigurationSuccess(ITestResult itr) {
		// TODO
	}
	
	
	@Override
	public void onConfigurationFailure(ITestResult itr) {
		try {
			if (BrowserFactory.getDriver() != null) {
				PfUtils.getWebScreenShot(BrowserFactory.getDriver());
				BrowserFactory.getDriver().quit();
			}
			ExtentManager.logTechnicalFail(itr.getThrowable().toString());
			Main.parameters.setResult("failure", "Configuration error : " + itr.getThrowable().toString(), itr.getThrowable().toString(), null, null);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void onConfigurationSkip(ITestResult itr) {
		// TODO
	}
	
	
	//@Override
	public void beforeConfiguration(ITestResult tr) {
		// TODO
	}

}