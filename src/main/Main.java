package main;

import com.relevantcodes.extentreports.LogStatus;
import extent.reports.ExtentManager;
import org.json.JSONException;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import utils.Parameters;
import utils.PfUtils;

import java.io.IOException;

@Listeners({ConfigurationListener.class})
public class Main {

    public static Parameters parameters;
    public static String workspacePath =
            (System.getProperty("user.dir").toLowerCase().contains("workspace")
                    ? System.getProperty("user.dir").substring(0, System.getProperty("user.dir").lastIndexOf("workspace") + "workspace".length())
                    : System.getProperty("user.dir")
            ) + System.getProperty("file.separator");
    public final static String reportName = "Report.html";
    public ExtentManager extentManager;
    public String screenShotFileName = "";

    @BeforeSuite
    public void setUp() throws IOException {
        // odczyt parametrów przekazanych z PFa
        parameters = new Parameters(workspacePath);
        parameters.readParameters();

        // start obiektu raportowego
        extentManager = new ExtentManager(workspacePath + "result" + System.getProperty("file.separator") + "attachments" + System.getProperty("file.separator") + reportName);
        extentManager.loadConfig("C:\\lib\\extent-config.xml");
        extentManager.startTest(parameters.getServiceName());

        // stworzenie obiektu driver
        BrowserFactory.setDriver();
    }


    // raportowanie po każdej metodzie z @Test
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result) throws IOException {
        if (result.getStatus() == ITestResult.FAILURE) {
            extentManager.exceptionHandler(result.getThrowable());
            if (screenShotFileName != null && !screenShotFileName.isEmpty())
                extentManager.logScreenShot(screenShotFileName, LogStatus.FAIL);
            PfUtils.getWebScreenShot(BrowserFactory.getDriver());
            parameters.setResult("failure", "Usługa zakończona ze statusem FAIL: szczegóły w zakładce 'Informacje techniczne'",		// "The service has been completed with status FAIL: more information in Technical information tab\n"
                    result.getThrowable().toString(), null , null);
        }
    }

    @AfterSuite
    public void tearDown(ITestContext context) throws JSONException, IOException {
        if (context.getFailedTests().size() == 0 && context.getPassedTests().size() > 0)
            parameters.setResult("success", "Usługa zakończona ze statusem PASS: szczegóły w bloku 'Informacje techniczne'",		// "The service has been completed with status PASS: more information in Technical information tab\n"
                    null, null, null);
        BrowserFactory.tearDownDriver();
        extentManager.closeReport();
    }

}
