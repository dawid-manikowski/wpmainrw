package steps.waits;

import main.BrowserFactory;

import static extent.reports.ExtentManager.logInfo;

public class Methods {

    public static void openPage(String url){
        BrowserFactory.getDriver().get(url);
        BrowserFactory.getDriver().manage().window().maximize();
        logInfo("Page: " + url +  " has been opened.");
    }

    public static void navigateToPage(String url){
        BrowserFactory.getDriver().navigate().to(url);
        logInfo("Tab with: " + url +  " has been opened.");
    }

}
